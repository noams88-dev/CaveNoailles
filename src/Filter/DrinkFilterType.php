<?php

namespace App\Filter;

use App\Entity\Category;
use App\Entity\Style;
use App\Entity\Subcategory;
use App\Repository\DrinkRepository;
use App\Service\SearchDrink;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DrinkFilterType extends AbstractType
{
    private DrinkRepository $drinkRepository;

    public function __construct(DrinkRepository $drinkRepository)
    {
        $this->drinkRepository = $drinkRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('keyword', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Rechercher par mots-clés'
                ]
            ])
            ->add('priceMin', HiddenType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Prix min'
                ]
            ])
            ->add('priceMax', HiddenType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Prix max'
                ]
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => false,
                'required' => false,
                'choice_label' => 'label',
                'placeholder' => 'Catégorie',
            ])
            ->add('subcategory', EntityType::class, [
                'class' => Subcategory::class,
                'label' => false,
                'required' => false,
                'choice_label' => 'label',
                'placeholder' => 'Sous-catégorie',
            ])
            ->add('styles', EntityType::class, [
                'class' => Style::class,
                'label' => 'Styles',
                'required' => true,
                'expanded' => true,
                'multiple' => true,
                'choice_label' => 'label',
            ])
            ->add('country', ChoiceType::class, [
                'label' => 'Pays',
                'required' => false,
                'choices'  => $this->drinkRepository->findCountryChoices(),
            ])
            ->add('numberOfResultsPerPage', ChoiceType::class, [
                'label' => 'Nombre de résultats par page',
                'required' => true,
                'choices'  => [
                    '15' => 15,
                    '25' => 25,
                    '50' => 50,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchDrink::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'filter';
    }
}
