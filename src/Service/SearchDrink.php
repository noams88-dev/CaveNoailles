<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Style;
use App\Entity\Subcategory;
use Doctrine\Common\Collections\Collection;

class SearchDrink
{
    public int $page = 1;

    public ?String $keyword = '';

    public int $priceMin;

    public int $priceMax;

    public Category $category;

    public Subcategory $subcategory;

    /**
     * @var Collection|Style[]
     */
    public $styles;

    public String $country;

    public int $numberOfResultsPerPage = 15;
}
