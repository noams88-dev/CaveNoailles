<?php

namespace App\Repository;

use App\Entity\Drink;
use App\Service\SearchDrink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Drink|null find($id, $lockMode = null, $lockVersion = null)
 * @method Drink|null findOneBy(array $criteria, array $orderBy = null)
 * @method Drink[]    findAll()
 * @method Drink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DrinkRepository extends ServiceEntityRepository
{
    private PaginatorInterface $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Drink::class);
        $this->paginator = $paginator;
    }

    public function searchByFilter(SearchDrink $searchDrink): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('drink')
            ->leftJoin('drink.category', 'category')
            ->leftJoin('drink.subcategory', 'subcategory')
            ->leftJoin('drink.styles', 'styles')
            ->select('drink', 'category', 'subcategory', 'styles');

        if (!empty($searchDrink->keyword)) {
            $query = $query
                ->orWhere('drink.name LIKE :keyword')
                ->orWhere('drink.producer LIKE :keyword')
                ->orWhere('drink.description LIKE :keyword')
                ->setParameter('keyword', "%$searchDrink->keyword%");
        }
        if (!empty($searchDrink->priceMin)) {
            $query = $query
                ->andWhere('drink.price >= :priceMin')
                ->setParameter('priceMin', $searchDrink->priceMin);
        }
        if (!empty($searchDrink->priceMax)) {
            $query = $query
                ->andWhere('drink.price <= :priceMax')
                ->setParameter('priceMax', $searchDrink->priceMax);
        }
        if (!empty($searchDrink->category)) {
            $query = $query
                ->andWhere('category = :category')
                ->setParameter('category', $searchDrink->category);
        }
        if (!empty($searchDrink->subcategory)) {
            $query = $query
                ->andWhere('subcategory = :subcategory')
                ->setParameter('subcategory', $searchDrink->subcategory);
        }
        if (!empty($searchDrink->styles) AND count($searchDrink->styles) > 0) {
            $query = $query
                ->andWhere('styles IN (:style)')
                ->setParameter('style', $searchDrink->styles);
        }
        if (!empty($searchDrink->country)) {
            $query = $query
                ->andWhere('drink.countryName LIKE :country')
                ->setParameter('country', $searchDrink->country);
        }
        $query = $query
            ->andWhere('drink.enable = 1');

        return $this->paginator->paginate($query, $searchDrink->page, $searchDrink->numberOfResultsPerPage);
    }

    public function findCountryChoices(): array
    {
        $arrayCountry = $this->createQueryBuilder('drink')
            ->select('drink.countryName')
            ->distinct()
            ->getQuery()
            ->getResult();

        $arrayCountryFormatted = [];
        foreach ($arrayCountry as $country) {
            if ($country['countryName'] != null) {
                $arrayCountryFormatted[$country['countryName']] = $country['countryName'];
            }
        }

        return $arrayCountryFormatted;
    }

    public function findMinMaxPrice(): array
    {
        $results = $this->createQueryBuilder('drink')
            ->select('MIN(drink.price) as min', 'MAX(drink.price) as max')
            ->getQuery()
            ->getScalarResult();

        return ['min' => (int)floor($results[0]['min']), 'max' => (int)ceil($results[0]['max'])];
    }
}
