<?php

namespace App\Controller;

use App\Entity\Drink;
use App\Filter\DrinkFilterType;
use App\Repository\DrinkRepository;
use App\Service\SearchDrink;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/drink")
 */
class DrinkController extends AbstractController
{
    /**
     * @Route("/", name="drink_index", methods={"GET"})
     */
    public function index(Request $request, DrinkRepository $drinkRepository): Response
    {
        $searchDrink = new SearchDrink();
        $searchDrink->page = $request->get('page', 1);

        $form = $this->createForm(DrinkFilterType::class, $searchDrink);
        $form->handleRequest($request);

        $drinks = $drinkRepository->searchByFilter($searchDrink);
        $minMaxPrice = $drinkRepository->findMinMaxPrice();

        return $this->renderForm('drink/index.html.twig', [
            'drinks' => $drinks,
            'minMaxPrice' => $minMaxPrice,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="drink_show", requirements={"id"="\d+"}, methods={"GET"})
     */
    public function show(Drink $drink): Response
    {
        return $this->render('drink/show.html.twig', [
            'drink' => $drink,
        ]);
    }
}
