<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Drink;
use App\Entity\Style;
use App\Entity\Subcategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;

/**
 * @Route("/admin")
 */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/", name="admin_index")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Cave de Noailles')
            ->setFaviconPath('images/compagny/logo.png')
            ->renderSidebarMinimized();
    }

    public function configureMenuItems(): iterable
    {
        return [
            // MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),
            MenuItem::linkToCrud('Catégories', 'fas fa-sitemap', Category::class),
            MenuItem::linkToCrud('Sous-catégories', 'fas fa-list', Subcategory::class),
            MenuItem::linkToCrud('Styles', 'fas fa-tags', Style::class),
            MenuItem::linkToCrud('Drinks', 'fas fa-wine-bottle', Drink::class),
        ];
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            ->displayUserName(false)
            ->addMenuItems([
                MenuItem::linkToRoute('Version public', 'fas fa-random', 'home_index'),
            ]);
    }
}
