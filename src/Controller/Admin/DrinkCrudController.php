<?php

namespace App\Controller\Admin;

use App\Entity\Drink;
use App\Form\ImageFormType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class DrinkCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Drink::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Drinks')
            ->setEntityLabelInSingular('un drink');

    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setCssClass('action-delete text-danger');
            })
            ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                return $action->setCssClass('action-delete btn btn-secondary pr-0 text-danger');
            });
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('category', 'Catégorie'))
            ->add(EntityFilter::new('subcategory', 'Sous-catégorie'))
            ->add(EntityFilter::new('styles', 'Styles'));
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->hideOnForm(),
            TextField::new('name', 'Nom'),

            // Different display for 'price'
            NumberField::new('price', 'Prix')
                ->setNumDecimals(2)
                ->formatValue(function ($value) {
                    return $value . ' €';
                }),
            NumberField::new('price', 'Prix (€)')
                ->setFormTypeOption('scale', 2)
                ->onlyOnForms(),

            // Different display for 'degree'
            NumberField::new('degree', 'Degrés')
                ->formatValue(function ($value) {
                    return $value . '°';
                }),
            NumberField::new('degree', 'Degrés (°)')
                ->setColumns('col-sm-6 col-lg-5 col-xxl-3')
                ->setFormTypeOption('scale', 1)
                ->onlyOnForms(),

            // Different display for 'capacity'
            IntegerField::new('capacity', 'Contenance')
                ->formatValue(function ($value) {
                    return $value . ' cl';
                }),
            IntegerField::new('capacity', 'Contenance (cl)')
                ->onlyOnForms(),

            TextField::new('year', 'Année'),
            CountryField::new('countryCode', 'Pays'),
            TextEditorField::new('description', 'Description'),
            TextField::new('producer', 'Producteur'),
            AssociationField::new('category', 'Catégorie'),
            AssociationField::new('subcategory', 'Sous-catégorie'),

            // Different display for 'styles'
            AssociationField::new('styles', 'Styles')
                ->setTemplatePath('admin/_modal_styles_drink_onIndex.html.twig')
                ->onlyOnIndex(),
            CollectionField::new('styles', 'Styles')
                ->onlyOnDetail(),
            AssociationField::new('styles', 'Styles')
                ->onlyOnForms(),

            // Different display for 'images'
            AssociationField::new('images', 'Images')
                ->setTemplatePath('admin/_modal_images_drink_onIndex.html.twig')
                ->onlyOnIndex(),
            AssociationField::new('images', 'Images')
                ->setTemplatePath('admin/_thumbnail_image_drinks_onDetail.html.twig')
                ->onlyOnDetail(),
            CollectionField::new('images', 'Images')
                ->setEntryType(ImageFormType::class)
                ->setFormTypeOption('by_reference', false)
                ->onlyOnForms(),

            // Different display for 'enable'
            BooleanField::new('enable', 'En ligne')
                ->setDisabled()
                ->onlyOnIndex(),
            BooleanField::new('enable', 'En ligne')
                ->onlyOnDetail(),
            BooleanField::new('enable', 'En ligne')
                ->onlyOnForms(),
        ];
    }
}
