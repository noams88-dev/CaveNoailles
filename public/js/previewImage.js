function previewImage(input, hauteur, largeur) {
    const images = input.files
    const siblings = input.parentNode.children;
    if (images) {
        // Supprime les previewImages déjà affichés
        for (let i = 0; i < siblings.length; i++) {
            if (siblings[i].tagName === 'IMG') {
                siblings[i].remove();
                i--;
            }
        }

        for (let i = 0; i < images.length; i++) {
            let img = document.createElement('img');
            img.src = URL.createObjectURL(images[i]);

            if (largeur) {
                img.style.width = largeur+'px';
            }
            else {
                img.style.width = '100%';
            }
            if (hauteur) {
                img.style.height = hauteur+'px';
            }
            img.style.objectFit = 'cover';
            input.parentNode.insertBefore(img, input);
        }
    }
}
